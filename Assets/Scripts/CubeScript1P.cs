﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeScript1P : MonoBehaviour {
    int Nsaltos=0;
	int Ncaidas = 0;
	float speedWalk=8f;
	float velMax = 8f;
	float salto;
	Rigidbody2D rgb;
	AudioSource clip;
	BoxCollider2D playerCollider;
	bool audioPause = false;
    GameObject Player, Audio;
	bool isRight = true;
	int dash=0;
	float dashSecondCounter = 0f;
	float dashTime = 0.5f;
	float volumen = 0.1f;
	bool isDashed = false;
	bool isStuned = false;
	bool isGrounded = false;
	float stunnedTime = 0f;
	float stunnedSecoundCounter = 0f;
	float secondsToCount = 1f;
	float secondsCounter = 0f;
	float progresiveVel = 7f;
	float secondsToMove = 1f;
	public KeyCode izquierda ;
	public KeyCode derecha;
	public KeyCode arriba;
	public KeyCode abajo;
	public KeyCode keyDash;
	KeyCode teclaPresionada;
	KeyCode teclaAnterior;
	// Use this for initialization

	void Start () {
		rgb = GetComponent<Rigidbody2D> ();
        //Player = GameObject.Find("player");
		Audio = GameObject.FindGameObjectWithTag("Audio");
		clip = Audio.GetComponent<AudioSource>();
		playerCollider = GetComponent<BoxCollider2D> ();
		clip.Play ();
		clip.loop = true;
		clip.volume = volumen;
    }
	
	// Update is called once per frame
	void Update () {
		AudioController ();
		ResetScene ();
		//if (!StunnedController()) {
			if (!isDashed) {
				Move ();
			} else {
				Dash ();
			}
	//	}
	}

	void FixedUpdate(){
		//AudioController ();
		//audio.Play ();
    }


	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag == "mapa" || coll.gameObject.tag == "wall") {
            Nsaltos = 0;
		}
		if (coll.gameObject.tag == "Player") {
			bool isOponentDashed = false;
			CubeScript1P oponent = coll.gameObject.GetComponent<CubeScript1P> ();

			/*if(oponent.isDashed && this.isDashed){
				isStuned = true;
				stunnedTime = 1.5f;
			}else if(oponent.isDashed){
				isStuned = true;
				stunnedTime = 2.5f;
			}
			print ("oponent dashed: " + oponent.isDashed + "; this.Dashed : " + isDashed);*/
			if (isDashed) {
				float moveCounter = 0f;
				isDashed = false;
				secondsToMove = 1f;
				isRight = !isRight;
				if (isRight) {
					moveCounter = 5f;
				} else {
					moveCounter = -5f;
				}
				MoveH (moveCounter);
			}
		}

    }
		
	void Move(){
		float speed= speedWalk;

		MoveHorizontal (speed);
		//IsGrounded ();
		Salto ();
		Caer ();
		DashController ();

		
	}

	void Caer(){
		if (Input.GetKey(abajo))
		{
			if (rgb.velocity.y >= -3f) {
				rgb.AddForce (new Vector2 (0, -2), ForceMode2D.Impulse);
			} else {
				rgb.velocity = new Vector2 (rgb.velocity.x, Mathf.Lerp (rgb.velocity.y, 0, Time.deltaTime));
			}
		}
	}

	void Salto(){
		if (Input.GetKeyDown(arriba))
		{
			/*if (isGrounded) {
				Nsaltos += 1;
				rgb.AddForce(new Vector2(0,5),ForceMode2D.Impulse );
			} else if (Nsaltos == 1) {
				rgb.AddForce(new Vector2(0,5),ForceMode2D.Impulse );
				Nsaltos = 0;
			}*/
			Nsaltos++;
			if (Nsaltos == 1 || Nsaltos == 2) {
				rgb.AddForce(new Vector2(0,5),ForceMode2D.Impulse );
			}
		}
	}

	void MoveHorizontal(float vel){
		if (Input.GetKey (derecha)) {
			MoveH (vel);
			isRight = true;
			Flip ();
		} else if (Input.GetKey (izquierda)) {
			MoveH (-vel);
			isRight = false;
			Flip ();
		} else {
			rgb.velocity = new Vector2 (Mathf.Lerp (rgb.velocity.x, 0, Time.deltaTime), rgb.velocity.y);
			vel = 0f;
		}
	}

	void MoveH(float vel){
		if (rgb.velocity.x <= velMax && !(isDashed)) {
			rgb.AddForce (Vector2.right * vel, ForceMode2D.Force);
		} else {
			rgb.velocity = new Vector2 (vel , rgb.velocity.y);
		}
	}

	void Dash(){
		secondsCounter += Time.deltaTime;
		if (secondsCounter < secondsToCount) {
			if (isRight) {
				MoveH (25f - progresiveVel);
				isRight = true; 
			} else {
				MoveH ((25f - progresiveVel)*(-1));
				isRight = false;
			}
			progresiveVel += Time.deltaTime*10;
		} else {
			secondsCounter = 0f;
			progresiveVel = 5f;
			isDashed = false;
		}
	}

	void DashController(){
		dashSecondCounter += Time.deltaTime;
		if (dashSecondCounter >= dashTime) {
			dash = 0;
			dashSecondCounter = 0f;
		}
		if (Input.GetKeyDown (keyDash)) {
			dash++;
			dashSecondCounter = 0f;
			if (dash == 2) {
				dash = 0;
				isDashed = true;
			}
		}
	}

	/*bool StunnedController(){
		if (isStuned) {
			stunnedSecoundCounter += Time.deltaTime;
			print (stunnedSecoundCounter + " : " + stunnedTime);
			if (stunnedSecoundCounter >= stunnedTime) {
				isStuned = false;
				stunnedSecoundCounter = 0f;
			}
		}
		return isStuned;
	}*/

	/*void IsGrounded (){
		isGrounded = Physics2D.Raycast (transform.position, Vector2.down, playerCollider.size.y + .05f);
		print (isGrounded);
	}*/

	void AudioController(){
		if (Input.GetKeyDown(KeyCode.KeypadPlus)) {
			if (volumen < 1.0f) {
				volumen += 0.1f;
			}
			clip.volume = volumen;
		}
		if (Input.GetKeyDown(KeyCode.KeypadMinus)) {
			if (volumen > 0.0f) {
				volumen -= 0.1f;
			}
			clip.volume = volumen;
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			if (!audioPause) {
				clip.Pause();
				audioPause = true;
			} else {
				clip.Play ();
				audioPause = false;
			}
		}
	}

	KeyCode TeclaPresionada(){
		if (Input.GetKeyDown (izquierda)) {
			return izquierda;
		} else if (Input.GetKeyDown (derecha)) {
			return derecha;
		} else if (Input.GetKeyDown (arriba)) {
			return arriba;
		} else if (Input.GetKeyDown (abajo)) {
			return abajo;
		} else {
			return KeyCode.Semicolon;
		}
	}

	void ResetScene(){
		if (Input.GetKeyDown(KeyCode.Y)){
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}

	void Flip(){
		var s = transform.localScale;
		s.x *= -1;
		transform.localScale = s;
	}

}
